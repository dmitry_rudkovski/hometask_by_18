package com.epam.tat.bo;

/**
 * Created by Я есть Грут on 16.08.2017.
 */
public class File {

    public String name;

    public File(String name) {
        this.name = name;
    }

    public File() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
