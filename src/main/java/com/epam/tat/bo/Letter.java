package com.epam.tat.bo;

/**
 * Created by Я есть Грут on 13.08.2017.
 */
public class Letter {

    private String address;

    private String topic;

    private String textMessage;

    public Letter(String address, String topic, String textMessage) {
        this.address = address;
        this.topic = topic;
        this.textMessage = textMessage;
    }

    public Letter() {

    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTextMessage() {
        return textMessage;
    }

    public void setTextMessage(String textMessage) {
        this.textMessage = textMessage;
    }
}
