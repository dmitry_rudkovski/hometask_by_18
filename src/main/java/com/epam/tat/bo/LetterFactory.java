package com.epam.tat.bo;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * Created by Я есть Грут on 13.08.2017.
 */
public class LetterFactory {

    public static Letter getGenerateLetter() {
        Letter letter = new Letter();
        letter.setAddress("rdv821@mail.ru");
        letter.setTextMessage("Text: " + RandomStringUtils.randomAlphabetic(5));
        letter.setTopic("Topic: " + RandomStringUtils.randomAlphabetic(8));
        return letter;
    }
}
