package com.epam.tat.exception;

import java.io.IOException;

/**
 * Created by Я есть Грут on 24.08.2017.
 */
public class CommonTestRuntimeException extends Exception {
    public CommonTestRuntimeException(String message, IOException e) {
        super(message,e);
    }
}
