package com.epam.tat.exception;

/**
 * Created by Я есть Грут on 13.08.2017.
 */
public class MailRuAuthenticationException extends RuntimeException {

    public MailRuAuthenticationException(String message) {
        super(message);
    }
}
