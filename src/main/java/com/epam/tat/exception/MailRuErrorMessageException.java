package com.epam.tat.exception;

/**
 * Created by Я есть Грут on 13.08.2017.
 */
public class MailRuErrorMessageException extends RuntimeException {

    public MailRuErrorMessageException(String message) {
        super(message);
    }
}
