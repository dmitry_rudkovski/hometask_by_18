package com.epam.tat.framework.appender;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;
import org.testng.Reporter;

/**
 * Created by Я есть Грут on 26.08.2017.
 */
public class TestNGReportAppender extends AppenderSkeleton {

    protected void append(LoggingEvent event) {
        Reporter.log(layout.format(event));
    }

    @Override
    public void close() {

    }

    @Override
    public boolean requiresLayout() {
        return false;
    }
}
