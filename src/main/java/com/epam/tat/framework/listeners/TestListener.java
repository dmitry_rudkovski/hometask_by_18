package com.epam.tat.framework.listeners;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.framework.ui.Browser;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

/**
 * Created by Я есть Грут on 23.08.2017.
 */
public class TestListener extends TestListenerAdapter{

    public void onStart(ITestContext testContext) {
        super.onStart(testContext);
        Log.info("[TEST STARTED] " + testContext.getName());
    }

    public void onFinish(ITestContext testContext) {
        super.onFinish(testContext);
        Log.info("[TEST FINISHED] " + testContext.getName());
        Log.debug("Quits this driver, closing every associated window");
        Browser.getInstance().stopBrowser();
    }

    public void onTestFailure(ITestResult tr) {
        Log.error("[Failed test]: " + tr.getTestName(), tr.getThrowable());
        Browser.getInstance().screenshot();
        Browser.getInstance().stopBrowser();
        super.onTestFailure(tr);
    }
}
