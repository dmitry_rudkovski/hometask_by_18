package com.epam.tat.framework.logging;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * Created by Я есть Грут on 23.08.2017.
 */
public class Log {

    static {
        PropertyConfigurator.configureAndWatch("./log4j.properties");
    }

    private static final Logger LOG = Logger.getLogger("com.epam.tat");

    public static void info(Object message) {
        LOG.info(message);
    }

    public static void error(Object message, Throwable t) {
        LOG.error(message, t);
    }

    public static void debug(Object message) {
        LOG.debug(message);
    }
}
