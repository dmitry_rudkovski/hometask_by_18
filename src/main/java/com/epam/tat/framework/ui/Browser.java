package com.epam.tat.framework.ui;

import com.epam.tat.exception.CommonTestRuntimeException;
import com.epam.tat.framework.logging.Log;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import java.io.File;
import java.io.IOException;
import java.util.Set;

/**
 * Created by Я есть Грут on 11.08.2017.
 */
public class Browser implements WrapsDriver {

    private WebDriver wrappedWebDriver;

    public static final int ELEMENT_VISIBILITY_TIMEOUT = 10;

    private Browser() {
        this.wrappedWebDriver = WebDriverFactory.getWebDriver();
    }

    private static ThreadLocal<Browser> instance = new ThreadLocal<>();

    public static synchronized Browser getInstance() {
        if (instance.get() == null) {
            instance.set(new Browser());
        }
        return instance.get();
    }

    public void stopBrowser() {
        try {
            if (wrappedWebDriver != null)
            wrappedWebDriver.quit();
        } finally {
            instance.set(null);
        }
    }

    @Override
    public WebDriver getWrappedDriver() {
        return wrappedWebDriver;
    }

    public void navigate(String url) {
        Log.debug("navigate " + url);
        wrappedWebDriver.get(url);
    }

    public void click(By by) {
        waitForAppear(by);
        wrappedWebDriver
                .findElement(by)
                .click();
    }

    public void type(By by, String text) {
        waitForAppear(by);
        wrappedWebDriver
                .findElement(by)
                .sendKeys(text);
    }

    public boolean isVisible(By by) {
        try {
            waitForAppear(by, 5);
        } catch (TimeoutException e) {
            return false;
        }
        return true;
    }

    public String getText(By by) {
        waitForAppear(by);
        return wrappedWebDriver.findElement(by).getText();
    }

    public boolean getEmptyText(By by) {
        waitForAppear(by);
        return wrappedWebDriver.findElement(by).getText().isEmpty();
    }

    public void switchTo(By by) {
        Log.debug("switchTo " + by);
        waitForAppear(by);
        wrappedWebDriver
                .switchTo()
                .frame(wrappedWebDriver.findElement(by));
    }

    public void switchToDefault() {
        Log.debug("switchToDefault");
        wrappedWebDriver
                .switchTo()
                .defaultContent();
    }

    public void uploadFile(File file,By fileInput) {
        Log.debug("uploadFile" + file);
        String pathToFile = file.getAbsolutePath();
        wrappedWebDriver
                .findElement(fileInput)
                .sendKeys(pathToFile);
    }

    public void switchToAlert() {
        Log.debug("switchToAlert");
        wrappedWebDriver
                .switchTo()
                .alert()
                .accept();
    }

    public void dragAndDrop(By from, By to) {
        Log.debug("dragAndDrop " + from + "to " + to);
        new Actions(wrappedWebDriver)
                .dragAndDrop(wrappedWebDriver.findElement(from), wrappedWebDriver.findElement(to))
                .build()
                .perform();
    }

    public void rightClick(By by) {
        Log.debug("rightClick " + by);
        waitForAppear(by);
        new Actions(wrappedWebDriver)
                .contextClick(wrappedWebDriver.findElement(by))
                .perform();
    }

    public void waitForAppear(By by, int timeout) {
        new WebDriverWait(wrappedWebDriver, timeout)
                .until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public void waitForAppear(By by) {
        Browser.getInstance().screenshot();
        Log.debug("waitForAppear: " + by);
        waitForAppear(by, ELEMENT_VISIBILITY_TIMEOUT);
    }

    public boolean verifyAlert() {
        Log.debug("verifyAlert");
        try {
            new WebDriverWait(wrappedWebDriver, 3)
                    .until(ExpectedConditions.alertIsPresent());
        } catch (TimeoutException e) {
            return false;
        }
        return true;
    }

    public void waitForAppearText(By by, String message) {
        waitForAppear(by);
        new WebDriverWait(wrappedWebDriver, ELEMENT_VISIBILITY_TIMEOUT)
                .until(ExpectedConditions.textToBe(by, message));
    }

    public void switchToView() {
        Set<String> windowHandles = wrappedWebDriver.getWindowHandles();
        for (String handle : windowHandles) {
            wrappedWebDriver.switchTo().window(handle);
        }
    }

    public void screenshot() {
        String screenshotName = System.nanoTime() + ".png";
        File screenshotFile = new File("logs/screenshots/" + screenshotName);
        byte[] screenshotBytes = ((TakesScreenshot) wrappedWebDriver).getScreenshotAs(OutputType.BYTES);
        try {
            Log.info("Screenshot taken: file://" + screenshotFile.getAbsolutePath());
            FileUtils.writeByteArrayToFile(screenshotFile, screenshotBytes);
            Reporter.log("<a href=" + screenshotFile.getAbsolutePath() + " target='blank' >" + screenshotName + "</a>");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
