package com.epam.tat.framework.ui;

/**
 * Created by Я есть Грут on 28.08.2017.
 */
public enum BrowserType {

    CHROME,
    FIREFOX;

    BrowserType() {
    }
}
