package com.epam.tat.framework.ui;

import com.epam.tat.framework.logging.Log;
import org.openqa.selenium.By;

/**
 * Created by Я есть Грут on 12.08.2017.
 */
public class Element {

    private By locator;

    public Element(By locator) {
        this.locator = locator;
    }

    public void type(String text) {
        Log.debug("Type " + locator);
        Browser.getInstance().type(locator, text);
    }

    public void click() {
        Log.debug("Click " + locator);
        Browser.getInstance().click(locator);
    }

    public String getText() {
        Log.debug("getText " + locator);
        return Browser.getInstance().getText(locator);
    }

    public void waitForAppear() {
        Log.debug("waitForAppear " + locator);
        Browser.getInstance().waitForAppear(locator);
    }

    public void waitForAppearText(String message) {
        Log.debug("waitForAppearText " + locator);
        Browser.getInstance().waitForAppearText(locator, message);
    }

    public boolean isVisible() {
        Log.debug("isVisible " + locator);
        return Browser.getInstance().isVisible(locator);
    }

    public boolean getEmptyText() {
        return Browser.getInstance().getEmptyText(locator);
    }

}
