package com.epam.tat.framework.ui;

import com.epam.tat.framework.runner.Parameters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.lang.reflect.Parameter;

/**
 * Created by Я есть Грут on 28.08.2017.
 */
public class WebDriverFactory {

    public static WebDriver getWebDriver() {
        WebDriver webDriver;
        switch (Parameters.instance().getBrowserType()) {
            case CHROME:
                System.setProperty("webdriver.chrome.driver", Parameters.instance().getChromeDriver());
                webDriver = new ChromeDriver();
                break;
            case FIREFOX:
                System.setProperty("webdriver.gecko.driver", Parameters.instance().getFirefoxDriver());
                webDriver = new FirefoxDriver();
                break;
            default:
                throw new RuntimeException("No support for: " + Parameters.instance().getBrowserType());
        }
        return webDriver;
    }
}
