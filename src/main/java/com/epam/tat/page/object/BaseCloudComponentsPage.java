package com.epam.tat.page.object;

import com.epam.tat.framework.ui.Browser;
import com.epam.tat.framework.ui.Element;
import org.openqa.selenium.By;
import java.io.File;
import static org.openqa.selenium.By.xpath;

/**
 * Created by Я есть Грут on 30.07.2017.
 */
public class BaseCloudComponentsPage {

    public static final String DYNAMIC_FOLDER_LOCATOR = "//div[@data-id='/%s' and @data-name='link']";
    public static final String DYNAMIC_CHECKBOX_LOCATOR = "//div[@data-id='/%s']//div[@class='b-checkbox__box']";
    public static final String FILE_PATH = "./src/main/resources/tat.txt";

    private static final Element createButton = new Element(xpath("//div[@title='Создать']"));
    private static final Element createFolder = new Element(xpath("//a[@data-name='folder']"));
    private static final Element nameOfFolder = new Element(xpath("//input[@class = 'layer__input']"));
    private static final Element addButton = new Element(xpath("//button[@data-name='add']"));
    private static final Element deleteButton = new Element(xpath("//div[@data-name='remove']"));
    private static final Element continueDeleteButton = new Element(xpath("//button[@data-name='remove']"));
    private static final Element popup = new Element(xpath("//div[@class='layer_trashbin-tutorial']"));
    private static final Element closeWindow = new Element(xpath("//div[@class='layer_trashbin-tutorial']/div[@class='layer__footer']/button[@data-name='close']"));
    private static final Element trashBox = new Element(xpath("//a[@href='/trashbin/']"));
    private static final Element uploadButton = new Element(xpath("//div[@data-name='upload']"));
    private static final By chooseFileButton = new By.ByXPath("//input[@class='layer_upload__controls__input' and(@type='file')] ");
    private static final By instanceOfFile = By.xpath("//div[@data-id ='/tat.txt']");
    private static final Element buttonToMove = new Element(xpath("//button[@data-name='move']"));
    private static final Element linkButton = new Element(xpath("//a[@data-name='publish']"));
    private static final Element showLinkToFile = new Element(xpath("//button[@data-name='popup']"));

    public BaseCloudComponentsPage clickCreateButton() {
        createButton.click();
        return this;
    }

    public BaseCloudComponentsPage clickCreateFolder() {
        createFolder.click();
        return this;
    }

    public BaseCloudComponentsPage clickAddButton() {
        addButton.click();
        return this;
    }

    public BaseCloudComponentsPage clickDeleteButton() {
        deleteButton.click();
        return this;
    }

    public BaseCloudComponentsPage finishDeleteButton() {
        continueDeleteButton.click();
        return this;
    }

    public void clickTrashbox() {
        trashBox.waitForAppear();
        trashBox.click();
    }

    public void clickCloseWindow() {
        closeWindow.click();
    }

    public BaseCloudComponentsPage clickUploadButton() {
        uploadButton.waitForAppear();
        uploadButton.click();
        return this;
    }

    public void clickButtonToMove() {
        buttonToMove.click();
    }

    public BaseCloudComponentsPage setNameFolder(String name) {
        nameOfFolder.type(name);
        return this;
    }

    public boolean waitInstanceOfFile() {
        return Browser.getInstance().isVisible(instanceOfFile);
    }

    public BaseCloudComponentsPage closePopUp() {
        popup.waitForAppear();
        clickCloseWindow();
        return this;
    }

    public BaseCloudComponentsPage clickChooseButtonFile() {
        Browser.getInstance().uploadFile(new File(FILE_PATH),chooseFileButton );
        return this;
    }

    public BaseCloudComponentsPage clickLinkButton() {
        linkButton.click();
        return this;
    }

    public BaseCloudComponentsPage clickShowLinkToFile() {
        showLinkToFile.click();
        return this;
    }

    public boolean isFolderCreated(String nameFolder) {
        String folder = String.format(DYNAMIC_FOLDER_LOCATOR, nameFolder);
        return Browser.getInstance().isVisible(By.xpath(folder));
    }

    public BaseCloudComponentsPage checkBoxClick(String nameFolder) {
        String checkBox = String.format(DYNAMIC_CHECKBOX_LOCATOR, nameFolder);
        Browser.getInstance().click(By.xpath(checkBox));
        return this;
    }

    public BaseCloudComponentsPage dragAndDrop(String nameFolder) {
        String folder = String.format(DYNAMIC_FOLDER_LOCATOR, nameFolder);
        Browser.getInstance().dragAndDrop(instanceOfFile, By.xpath(folder));
        return this;
    }

    public BaseCloudComponentsPage rightClick(String nameFolder) {
        String folder = String.format(DYNAMIC_FOLDER_LOCATOR, nameFolder);
        Browser.getInstance().rightClick(By.xpath(folder));
        return this;
    }
}
