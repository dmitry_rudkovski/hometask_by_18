package com.epam.tat.page.object;

import com.epam.tat.framework.ui.Browser;
import com.epam.tat.framework.ui.Element;
import static org.openqa.selenium.By.xpath;

/**
 * Created by Я есть Грут on 30.07.2017.
 */
public class CloudLoginPage {

    private final String cloudUrl = "https://cloud.mail.ru/";

    private static final Element loginButton = new Element(xpath("//input[@class = 'nav-inner-col-try__bt']"));
    private static final Element loginForm = new Element(xpath("//form[@id ='x-ph__authForm__popup']"));
    private static final Element loginField = new Element(xpath("//input[@id='ph_login']"));
    private static final Element passwordField = new Element(xpath("//input[@id ='ph_password']"));
    private static final Element submitButton = new Element(xpath("//span[@data-action='login']"));
    private static final Element messageErrorFrame = new Element(xpath("//iframe[@class='ag-popup__frame__layout__iframe']"));
    private static final Element errorLoginMessage = new Element(xpath("//div[@class='b-login__errors']"));
    private static final Element mailUser = new Element(xpath("//i[@id='PH_user-email']"));

    public CloudLoginPage open() {
        Browser.getInstance().navigate(cloudUrl);
        return this;
    }

    public void clickLoginButton() {
        loginButton.click();
    }

    public CloudLoginPage setLogin(String login) {
        loginField.type(login);
        return this;
    }

    public CloudLoginPage setPassword(String password) {
        passwordField.type(password);
        return this;
    }

    public void clickSubmitButton() {
       submitButton.click();
    }


}
