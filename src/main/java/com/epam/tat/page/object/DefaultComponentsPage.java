package com.epam.tat.page.object;

import com.epam.tat.framework.ui.Element;
import static org.openqa.selenium.By.cssSelector;
import static org.openqa.selenium.By.xpath;

/**
 * Created by Я есть Грут on 25.07.2017.
 */
public class DefaultComponentsPage {

    private static final Element writeLetter = new Element(cssSelector("a[data-name = 'compose']"));
    private static final Element comingMessage = new Element(xpath("//div[@class='message-sent__title']/a[@href='/messages/inbox/']"));
    private static final Element draftMessage = new Element(xpath("//a[@href='/messages/drafts/']"));

    public void writeNewLetter() {
        writeLetter.click();
    }

    public void clickComingMessage() {
        comingMessage.click();
    }

    public void clickDraftMessage() {
        draftMessage.waitForAppear();
        draftMessage.click();
    }
}
