package com.epam.tat.page.object;

import com.epam.tat.framework.ui.Element;
import static org.openqa.selenium.By.xpath;

/**
 * Created by Я есть Грут on 28.07.2017.
 */
public class InboxAndSentMessagePage {

    private static final Element contentMessage = new Element(xpath("//div[@class ='b-datalist__item__subj']/span[@class='b-datalist__item__subj__snippet']"));

    public String getContentMessage() {
        return contentMessage.getText();
    }

    public void waitContentMessage(String message) {
        contentMessage.waitForAppearText(message);
    }

}
