package com.epam.tat.page.object;

import com.epam.tat.framework.ui.Element;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.By.xpath;

/**
 * Created by Я есть Грут on 01.08.2017.
 */
public class InnerCloudElementPage {

    private static final Element visabilityOfFile = new Element(xpath("//div[@class='b-thumb__content']"));

    @FindBy(xpath = "//div[@data-id='/tat.txt']/div[2]/div[2]/div/div/span[@class='b-filename__name']")
    private WebElement fileNameOfFile;

    public String textFileNameOfFile() {
        return fileNameOfFile.getText();
    }

    public boolean visabilityOfFile() {
        return visabilityOfFile.isVisible();
    }
}
