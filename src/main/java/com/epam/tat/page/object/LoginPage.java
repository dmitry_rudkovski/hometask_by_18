package com.epam.tat.page.object;

import com.epam.tat.framework.ui.Browser;
import com.epam.tat.framework.ui.Element;
import static org.openqa.selenium.By.id;
import static org.openqa.selenium.By.name;
import static org.openqa.selenium.By.xpath;

/**
 * Created by Я есть Грут on 24.07.2017.
 */
public class LoginPage {

    private static final String URL = "https://mail.ru/";

    private static final Element loginField = new Element(name("Login"));
    private static final Element passwordField = new Element(name("Password"));
    private static final Element submitButton = new Element(id("mailbox__auth__button"));
    private static final Element mailUser = new Element(xpath("//*[@id='PH_user-email']"));
    private static final Element errorMessage = new Element(xpath("//*[@id='mailbox:authfail']"));


    public LoginPage open() {
        Browser.getInstance().navigate(URL);
        return this;
    }

    public LoginPage setLogin(String login) {
        loginField.type(login);
        return this;
    }

    public LoginPage setPassword(String password) {
        passwordField.type(password);
        return this;
    }

    public void clickSubmitButton() {
        submitButton.click();
    }

    public boolean confirmLogin() {
        return mailUser.isVisible();
    }

    public String getValueMailUser() {
        return mailUser.getText();
    }

    public boolean visibleErrorMessage() {
        return errorMessage.isVisible();
    }

    public String getErrorMessageInField() {
        return errorMessage.getText();
    }
}
