package com.epam.tat.page.object;

import com.epam.tat.framework.ui.Browser;
import com.epam.tat.framework.ui.Element;
import org.openqa.selenium.By;

import static org.openqa.selenium.By.cssSelector;
import static org.openqa.selenium.By.xpath;

/**
 * Created by Я есть Грут on 25.07.2017.
 */
public class NewLetterPage {

    private static final Element addressField = new Element(cssSelector("textarea[data-original-name='To']"));
    private static final Element topicField = new Element(xpath("//input[@class='b-input']"));
    private static final Element textField = new Element(cssSelector("#tinymce"));
    private static final Element sendButton = new Element(xpath("//div[@data-name='send']/span"));
    private static final By messageFrame = By.xpath("//iframe[starts-with(@id,'toolkit-')]");
    private static final Element continueButton = new Element(xpath("//div[@class ='is-compose-empty_in']/form/div[@class ='popup__controls']/button[@type ='submit']"));
    private static final Element saveDraft = new Element(xpath("//div[@data-name ='saveDraft']"));
    private static final Element popUp = new Element(cssSelector(".popup.js-layer.popup_dark.popup_"));

    public NewLetterPage setAddressField(String address) {
        addressField.type(address);
        return this;
    }

    public boolean getEmptyAddressField() {
        return addressField.getEmptyText();
    }

    public NewLetterPage setTopicField(String topic) {
        topicField.type(topic);
        return this;
    }

    public boolean popUpIsVisible() {
         return popUp.isVisible();
    }

    public NewLetterPage setTextField(String text) {
        Browser.getInstance().switchTo(messageFrame);
        textField.type(text);
        Browser.getInstance().switchToDefault();
        return this;
    }

    public void sendMessage() {
        sendButton.click();
    }

    public void continueSendMessage() {
        continueButton.click();
    }

    public void saveDraftMail() {
        saveDraft.click();
    }
}
