package com.epam.tat.page.object;

import com.epam.tat.framework.ui.Element;

import static org.openqa.selenium.By.xpath;

/**
 * Created by Я есть Грут on 30.07.2017.
 */
public class TrashboxPage {

    private static final Element clearBox = new Element(xpath("//div[@data-name='clear']/span"));
    private static final Element finallyTrash = new Element(xpath("//button[@data-name='empty']"));
    private static final Element trashEmptyMessage = new Element(xpath("//div[@class='b-datalist-message__text']"));

    public TrashboxPage clickClearBox() {
        clearBox.click();
        return this;
    }

    public TrashboxPage clickFinallyTrash() {
        finallyTrash.click();
        return this;
    }

    public String getTrashEmptyMessage() {
        return trashEmptyMessage.getText();
    }
}

