package com.epam.tat.service;

import com.epam.tat.bo.Account;
import com.epam.tat.exception.MailRuAuthenticationException;
import com.epam.tat.framework.logging.Log;
import com.epam.tat.page.object.CloudLoginPage;
import com.epam.tat.page.object.LoginPage;

/**
 * Created by Я есть Грут on 10.08.2017.
 */
public class AuthenticationService {

    public void loginIn(Account account) {
        Log.debug(String.format("Login with account: %s | %s", account.getEmail(), account.getPassword()));
        LoginPage loginPage = new LoginPage().open();
        loginPage
                .setLogin(account.getEmail())
                .setPassword(account.getPassword())
                .clickSubmitButton();
        if (loginPage.visibleErrorMessage()) {
            throw new MailRuAuthenticationException(loginPage.getErrorMessageInField());
        }
        if (!loginPage.confirmLogin()) {
            throw new MailRuAuthenticationException("Is not logging");
        }
    }

    public void loginInCloud(Account account) {
        Log.debug(String.format("Login with account: %s | %s", account.getEmail(), account.getPassword()));
        CloudLoginPage cloudLoginPage = new CloudLoginPage().open();
        cloudLoginPage.clickLoginButton();
        cloudLoginPage
                .setLogin(account.getEmail())
                .setPassword(account.getPassword())
                .clickSubmitButton();
    }
}
