package com.epam.tat.service;

import com.epam.tat.bo.File;
import com.epam.tat.framework.ui.Browser;
import com.epam.tat.page.object.BaseCloudComponentsPage;
import com.epam.tat.page.object.InnerCloudElementPage;
import com.epam.tat.page.object.TrashboxPage;

/**
 * Created by Я есть Грут on 16.08.2017.
 */
public class FileService {


    public void addFolder(File file) {
        BaseCloudComponentsPage baseCloudComponentsPage = new BaseCloudComponentsPage();
        baseCloudComponentsPage
                .clickCreateButton()
                .clickCreateFolder()
                .setNameFolder(file.getName())
                .clickAddButton();

    }

    public boolean isFolderCreated(File file) {
        BaseCloudComponentsPage baseCloudComponentsPage = new BaseCloudComponentsPage();
        return baseCloudComponentsPage.isFolderCreated(file.getName());
    }

    public void deleteFolder(File file) {
        BaseCloudComponentsPage baseCloudComponentsPage = new BaseCloudComponentsPage();
        baseCloudComponentsPage
                .checkBoxClick(file.getName())
                .clickDeleteButton()
                .finishDeleteButton()
                .closePopUp()
                .clickTrashbox();
        TrashboxPage trashboxPage = new TrashboxPage();
        trashboxPage.clickClearBox()
                .clickFinallyTrash()
                .getTrashEmptyMessage();
    }

    public void uploadFile(File file) {
        BaseCloudComponentsPage baseCloudComponentsPage = new BaseCloudComponentsPage();
        baseCloudComponentsPage
                .clickUploadButton()
                .clickChooseButtonFile()
                .waitInstanceOfFile();
    }

    public void dragAndDrop(File file) {
        BaseCloudComponentsPage baseCloudComponentsPage = new BaseCloudComponentsPage();
        baseCloudComponentsPage
                .dragAndDrop(file.getName())
                .clickButtonToMove();
    }

    public void rightClickAndShare(File file) {
        BaseCloudComponentsPage baseCloudComponentsPage = new BaseCloudComponentsPage();
        baseCloudComponentsPage
                .isFolderCreated(file.getName());
        baseCloudComponentsPage
                .rightClick(file.getName())
                .clickLinkButton()
                .clickShowLinkToFile();
        Browser.getInstance().switchToView();

    }

    public String trashEmptyMessage(File file) {
        TrashboxPage trashboxPage = new TrashboxPage();
        return trashboxPage.getTrashEmptyMessage();
    }

    public boolean visabilityOfFile() {
        InnerCloudElementPage innerCloudElementPage = new InnerCloudElementPage();
        return innerCloudElementPage.visabilityOfFile();
    }
}
