package com.epam.tat.service;

import com.epam.tat.bo.Letter;
import com.epam.tat.exception.MailRuErrorMessageException;
import com.epam.tat.framework.logging.Log;
import com.epam.tat.framework.ui.Browser;
import com.epam.tat.page.object.DefaultComponentsPage;
import com.epam.tat.page.object.InboxAndSentMessagePage;
import com.epam.tat.page.object.NewLetterPage;

/**
 * Created by Я есть Грут on 13.08.2017.
 */
public class LetterService {

    public void writeNewLetter(Letter letter) {
        Log.debug(String.format("Write new letter with data: %s | %s | %s", letter.getAddress(), letter.getTopic(), letter.getTextMessage()));
        DefaultComponentsPage defaultComponentsPage = new DefaultComponentsPage();
        defaultComponentsPage.writeNewLetter();
        NewLetterPage newLetterPage = new NewLetterPage();
        newLetterPage
                .setAddressField(letter.getAddress())
                .setTopicField(letter.getTopic())
                .setTextField(letter.getTextMessage())
                .sendMessage();
        if (Browser.getInstance().verifyAlert()) {
            Browser.getInstance().switchToAlert();
            if (newLetterPage.getEmptyAddressField()) {
                throw new MailRuErrorMessageException("Not address");
            }
        }
        if (newLetterPage.popUpIsVisible()) {
            newLetterPage.continueSendMessage();
        }
        else{
            defaultComponentsPage.clickComingMessage();
        InboxAndSentMessagePage inboxAndSentMessagePage = new InboxAndSentMessagePage();
        inboxAndSentMessagePage.waitContentMessage(letter.getTextMessage());
        }
    }

    public void writeLetterInDraft(Letter letter) {
        Log.debug(String.format("Write new letter with data: %s | %s | %s", letter.getAddress(), letter.getTopic(), letter.getTextMessage()));
        DefaultComponentsPage defaultComponentsPage = new DefaultComponentsPage();
        defaultComponentsPage.writeNewLetter();
        NewLetterPage newLetterPage = new NewLetterPage();
        newLetterPage
                .setAddressField(letter.getAddress())
                .setTopicField(letter.getTopic())
                .setTextField(letter.getTextMessage())
                .saveDraftMail();
        defaultComponentsPage.clickDraftMessage();
        InboxAndSentMessagePage inboxAndSentMessagePage = new InboxAndSentMessagePage();
        inboxAndSentMessagePage.waitContentMessage(letter.getTextMessage());
    }
}
