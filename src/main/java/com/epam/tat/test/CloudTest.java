package com.epam.tat.test;

import com.epam.tat.bo.Account;
import com.epam.tat.bo.AccountFactory;
import com.epam.tat.bo.File;
import com.epam.tat.bo.FileFactory;
import com.epam.tat.framework.ui.Browser;
import com.epam.tat.service.AuthenticationService;
import com.epam.tat.service.FileService;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by Я есть Грут on 30.07.2017.
 */
public class CloudTest extends BaseClassTest {

    private String nameFolder;

    private AuthenticationService authenticationService;
    private FileService fileService;

    @BeforeMethod
    public void preconditions() {
        Browser browser = Browser.getInstance();
        authenticationService = new AuthenticationService();
        fileService = new FileService();
    }

    @Test(description = "Test login cloud form with correct email and password")
    public void cloudLoginTest() {
        Account account = AccountFactory.getExistentAccount();
        authenticationService.loginInCloud(account);
    }

    @Test(description = "Test create folder in cloud")
    public void createFolder() {
        Account account = AccountFactory.getExistentAccount();
        authenticationService.loginInCloud(account);
        File file = FileFactory.getGenerateFile();
        fileService.addFolder(file);
        Assert.assertTrue(fileService.isFolderCreated(file));
    }

    @Test(description = "Test create folder and delete it from anything in cloud")
    public void createAndDeleteFolder() {
        Account account = AccountFactory.getExistentAccount();
        authenticationService.loginInCloud(account);
        File file = FileFactory.getGenerateFile();
        fileService.addFolder(file);
        fileService.deleteFolder(file);
        Assert.assertEquals(fileService.trashEmptyMessage(file), EMPTY_TRASH_MESSAGE);
    }

    @Test(description = "Test drag and drop file from root folder to new folder")
    public void dragAndDropFile() throws InterruptedException {
        Account account = AccountFactory.getExistentAccount();
        authenticationService.loginInCloud(account);
        File file = FileFactory.getGenerateFile();
        fileService.addFolder(file);
        fileService.uploadFile(file);
        fileService.dragAndDrop(file);
        Assert.assertTrue(fileService.isFolderCreated(file));
    }

    @Test(description = "Test share file in cloud")
    public void shareFile() {
        Account account = AccountFactory.getExistentAccount();
        authenticationService.loginInCloud(account);
        File file = FileFactory.getGenerateFile();
        fileService.addFolder(file);
        fileService.uploadFile(file);
        fileService.dragAndDrop(file);
        fileService.rightClickAndShare(file);
        Assert.assertTrue(fileService.visabilityOfFile());
    }

    @AfterMethod
    public static void quitOut() {
        Browser.getInstance().stopBrowser();
    }
}
