package com.epam.tat.test;

import com.epam.tat.bo.Account;
import com.epam.tat.bo.AccountFactory;
import com.epam.tat.bo.Letter;
import com.epam.tat.bo.LetterFactory;
import com.epam.tat.exception.MailRuAuthenticationException;
import com.epam.tat.exception.MailRuErrorMessageException;
import com.epam.tat.framework.listeners.TestListener;
import com.epam.tat.framework.ui.Browser;
import com.epam.tat.service.AuthenticationService;
import com.epam.tat.service.LetterService;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

/**
 * Created by Я есть Грут on 24.07.2017.
 */

public class MainTest extends BaseClassTest {

    private AuthenticationService authenticationService;
    private LetterService letterService;

    @BeforeMethod
    public void preconditions() {
        Browser browser = Browser.getInstance();
        authenticationService = new AuthenticationService();
        letterService = new LetterService();
    }

    @Test(description = "Login form with correct email and password")
    public void loginTest() {
        Account account = AccountFactory.getExistentAccount();
        authenticationService.loginIn(account);
    }

    @Test(description = "Login form with correct only email or password",
            expectedExceptions = MailRuAuthenticationException.class)
    public void incorrectLoginTest() {
        Account account = AccountFactory.getExistentAccount();
        account.setPassword("123");
        authenticationService.loginIn(account);
    }

    @Test(description = "Login form with both empty fields or empty email input",
            expectedExceptions = MailRuAuthenticationException.class)
    public void incorrectLoginTestWithEmptyEmailFieldOrBoth() {
        Account account = AccountFactory.getExistentAccount();
        account.setPassword("");
        account.setEmail("");
        authenticationService.loginIn(account);
    }

    @Test(description = "Login form with correct email and password, write new letter and check it")
    public void loginTestAndCheck() {
        Account account = AccountFactory.getExistentAccount();
        authenticationService.loginIn(account);
        Letter letter = LetterFactory.getGenerateLetter();
        letterService.writeNewLetter(letter);
    }

    @Test(description = "Write new letter only without address",
            expectedExceptions = MailRuErrorMessageException.class)
    public void writeNewLetterWithoutAddress() {
        Account account = AccountFactory.getExistentAccount();
        authenticationService.loginIn(account);
        Letter letter = LetterFactory.getGenerateLetter();
        letter.setAddress("");
        letterService.writeNewLetter(letter);

    }

    @Test(description = "Write new letter with correct address field and empty topic and text field")
    public void writeNewLetterWithoutTopicAndMessage() {
        Account account = AccountFactory.getExistentAccount();
        authenticationService.loginIn(account);
        Letter letter = LetterFactory.getGenerateLetter();
        letter.setTopic("");
        letter.setTextMessage("");
        letterService.writeNewLetter(letter);
    }

//    @Test(description = "Save message into draft section")
//    public void draftMailTest() {
//        Account account = AccountFactory.getExistentAccount();
//        authenticationService.loginIn(account);
//        Letter letter = LetterFactory.getGenerateLetter();
//        letterService.writeLetterInDraft(letter);
//    }

    @AfterMethod
    public static void quitOut() {
        Browser.getInstance().stopBrowser();
    }
}
